---
layout: default
title: Overview
---

This website introduces the FullMonte Project. The FullMonte Project was created by the medical biophotonics research group and the computer engineering group of the University of Toronto. The project aims to enable faster light propagation simulations for various medical applications through interdisciplinary cooperation. The project consists of three distinct software tools:
1. [FullMonteSW](https://gitlab.com/FullMonte/FullMonteSW/wikis/home): The fastest open-source tetrahedral mesh-based Monte Carlo software model available. 
2. [MeshTool](https://gitlab.com/FullMonte/MeshTool/wikis/home): A tool that allows the meshing of individually segmented layers of medical images into a complete mesh model that serves as input to FullMonte.
3. [PDT-SPACE](https://gitlab.com/FullMonte/pdt-space/-/wikis/Home): A treatment optimization tool for Photodynamic Therapy that uses FullMonte to optimize light distributions to reduces healthy tissue damage.

## Overall Workflow

<img src="/img/workflow.png" alt="FullMonte Project Workflow" width="700px" style="float:center"/>

The general workflow when using FullMonte is depicted in the above picture. We provide a docker image package that includes all necessary software to start using the FullMonte project to its full capabilities. To learn more about how to install and run this docker image that includes the major FullMonte tools (FullMonteSW, MeshTool and PDT-SPACE) and the libraries they require, go to [FullMonteMeta](https://gitlab.com/FullMonte/fullmonte-meta). We also provide [FullMonteWeb](http://fullmontesuite.herokuapp.com/application/) to let you specify problems using a user-friendly web interface and launch simulations on AWS instances in the cloud so you don't need to set up any software or computers. 


## Capabilities and Usage ##
Modeling of light propagation through turbid (highly-scattering) media such as living tissue is important for a number of medical applications including diagnostics such as diffuse optical tomography and therapeutics such as photodynamic therapy. The FullMonte Software can perform the simulations required in these and many other applications quickly and accurately. 

<img src="/img/rat.png" alt="Volume Fluence" width="350px" style="float:right"/>

As input, the Geometry Specifications (a tetrahedral mesh) and Material Optical properties should be provided, along with the optical source description. FullMonte has file readers for several popular mesh formats, and the source repository includes utilities to leverage the cgal tool to create new meshes. FullMonte also supports different optical sources, such as point sources, spherical sources and composite sources. FullMonte will simulate the light propagation and absorption throughout the geometry and can output a wide variety of data, including the light absorption and fluence throughout the geometry, and traces of photon packets. FullMonte also includes links to allow easy visualization of the results in VTK.

<img src="/img/IsoIntensity.png" alt="Iso Intensity" width="350px" style="float:right"/>

FullMonte's accuracy has been extensively validated against other simulators, and it has been programmed to use multi-threading, cache-friendly data structures and the SIMD instructions of recent Intel processors so that it achieves the highest performance of any open-source tetrahedral mesh Monte Carlo photonic simulator. By using a tetrahedral mesh, FullMonte can accurately model complex geometries and surface normals, improving accuracy for simulations where reflection and refraction within the material are important.

FullMonte also provides a built-in Tcl scripting interface, so advanced users can create custom scripts and flows that use the FullMonte simulator to rapidly analyze many related cases or perform other customized runs. The FullMonte source code has also been written to be highly modular and has extensive unit tests that are run automatically after every code change, making it easier and safer for C++ developers to extend its capabilities further.

PDT-SPACE builds on FullMonte to automatically created an optimized treatment plan for Photodynamic Therapy, by intelligently searching the space of possible light source locations and using FullMonte to evaluate the various promising choices.

MeshTool is a helpful utility for converting input geometric data into the mesh format used by FullMonte and PDT-SPACE.

## Getting Started, Installing and Running ##
To get started with FullMonte and/or the other tools we have created that augment or support FullMonte see [Overview](https://gitlab.com/FullMonte/FullMonteSW/wikis/home).

Our group's full suite of tools related to modeling light propagation and optimizing light distributions for photodynamic therapy are listed and available [here](https://gitlab.com/FullMonte).


If you don't want to install anything, you can try our [web interface](http://fullmontesuite.herokuapp.com/application/) that would run the simulations on an Amazon EC2 server. 

## Background and How to Cite ##

The basic operation of FullMonte and the best paper to cite if you are using it in your work is available [here](/Using/) .

More information on the operation of the FullMonte simulator is available in this [paper on PDT plan evaluation](https://www.frontiersin.org/articles/10.3389/fphy.2015.00006/full).

To read about how FullMonte enables PDT treatment plan optimization with PDT-SPACE, see this [paper on PDT optimization](https://doi.org/10.1364/BOE.9.000898).

There are many other publications on enhancements and uses of FullMonte and PDT-SPACE: [full publication list](https://gitlab.com/FullMonte/FullMonteSW/-/wikis/publication-list#citations)

