---
layout: page
title: Using
permalink: Using/
---

For an extensive list of the FullMonte Project's publications, please visit the following link:
* [**Publications**](https://gitlab.com/FullMonte/FullMonteSW/-/wikis/Publication-List)

How to Cite
===========
The following [paper](http://www.eecg.utoronto.ca/~vaughn/papers/spie2015_fullmonte_treatment.pdf) may be used as a general citation for fullmonte software (bibtex format):

    @inproceedings{schwiegelshohn2019fullmonte,
      title={FullMonte: Fast Monte Carlo Light Simulator},
      author={Schwiegelshohn, Fynn and Young-Schultz, Tanner and Afsharnejad, Yasmin and Molenhuis, Daniel and Lilge, Lothar and Betz, Vaughn},
      booktitle={European Conference on Biomedical Optics},
      pages={11079\_36},
      year={2019},
      organization={Optical Society of America}
    }

Docker deployment
=================

The preferred way of running FullMonte on Linux, Mac, or Windows is via the Docker deployment. 
You can [install Docker](https://www.docker.com) for free via a download or a Linux package.
We manage the project such that the master (latest) branch is continuously tested and deployed using [Gitlab CI/CD]() and you can see its status at any time.

You can see the available Docker images, with some information on how to run them in the [FullMonte's Gitlab Docker registry](https://gitlab.com/FullMonte/FullMonteSW/container_registry).
There are also instructions in the [wiki](https://gitlab.com/FullMonte/FullMonteSW/wikis/Install).


Development Trunk
=================
For those wishing to build on top of FullMonte, the development trunk is hosted at [Gitlab](https://gitlab.com/FullMonte/FullMonteSW/tree/master).


Documentation
=============

Please have a look at the [wiki](https://gitlab.com/FullMonte/FullMonteSW/wikis/home) and [helpful code snippets](https://gitlab.com/FullMonte/FullMonteSW/snippets) to orient yourself.


Issue tracking
==============

The code and documentation can't improve if we don't know where defects are. 
If something isn't working as planned, please let us know. 
We track issues (crashes, documentation errors/omissions, feature requests, inaccuracy) in the [Gitlab issue tracker](https://gitlab.com/FullMonte/FullMonteSW/issues). 
Please check for an existing issue that matches yours and consult the [wiki page about bug reports](https://gitlab.com/FullMonte/FullMonteSW/wikis/Reporting-bugs) before submitting.
Thanks!

