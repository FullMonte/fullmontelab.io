---
layout: page
title: Outreach
permalink: Outreach/
---

## Gummy Bears and Lasers:

**Experiments with Light Absorption, Scattering and Total Internal Reflection**

The kit, experiments and videos develop an in-class experiment and discussion on how light interacts with different materials and how those interactions enable fiber optics and lead to different absorption of light by different materials. 

The light absorption and reflection effects demonstration is also linked to a medical application of light being actively researched at UHN and the University of Toronto in which light is guided to a tumour with fiber optics and where the light is absorbed the tumour is destroyed.

The lesson is primarily targeted at high school level students, but could also be applied at earlier grades. To obtain a kit with the lasers and other materials for the lesson, please contact Chris McFadden at <christopher.mcfadden@ddsb.ca>; alternate contacts are <vaughn@eecg.utoronto.ca> or <llilge@uhnresearch.ca>.

<img src="/img/totalInternalReflection.jpg" alt="Total Internal Refletion" width="774px" style="float:center"/>

<img src="/img/fiberOptic.jpg" alt="Water Fiber Optics" width="602px" style="float:center"/>

<img src="/img/gummyBearLaser.jpg" alt="Lasers on Gummy Bears" width="550px" style="float:center"/>


## Lesson Plan and Handouts
[Lesson Plan](https://gitlab.com/FullMonte/fullmonte.gitlab.io/-/blob/master/outreachDocs/LessonPlan.docx)

[Power Point of Lesson](https://gitlab.com/FullMonte/fullmonte.gitlab.io/-/blob/master/outreachDocs/LessonPowerpoint.pptx)

[Contents of Kit](https://gitlab.com/FullMonte/fullmonte.gitlab.io/-/blob/master/outreachDocs/KitPackingList.docx)

[Reflection Observations Handout for Class](https://gitlab.com/FullMonte/fullmonte.gitlab.io/-/blob/master/outreachDocs/ReflectionObservationsHandout.doc)

[Gummy Bear Observations Handout for Class](https://gitlab.com/FullMonte/fullmonte.gitlab.io/-/blob/master/outreachDocs/GummyBearObservationsHandout.docx)

## Videos

[Tutorial 1: Unboxing and Required Materials](https://youtu.be/Be_1z6d7J88)

<iframe width="560" height="315" src="https://www.youtube.com/embed/Be_1z6d7J88" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

[Tutorial 2: Total Internal Reflection Demo](https://youtu.be/bn88ysohMvI)

<iframe width="560" height="315" src="https://www.youtube.com/embed/bn88ysohMvI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

[Tutorial 3: Fibre Optic Demo](https://youtu.be/o6zaCwDeSL8)

<iframe width="560" height="315" src="https://www.youtube.com/embed/o6zaCwDeSL8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

[Tutorial 4: Gummy Bears](https://youtu.be/xrQZctMO23c)

<iframe width="560" height="315" src="https://www.youtube.com/embed/xrQZctMO23c" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Acknowledgements and Funding Support

This lesson and all the associated materials were created by Chris McFadden, who is both a high school teacher in the Durham District School Board and a graduate student (and member of the FullMonte team) in the Department of Medical Biophysics at the University of Toronto. Funding support was provided by the Ontario Research Foundation. We are thankful to Shawn Brooks of the University of Toronto Schools for facilitating and hosting pilots of the lesson.

<img src="/img/ontario-research-fund.png" alt="Ontario Research Fund" width="480px" style="float:center"/>
